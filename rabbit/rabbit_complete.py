import requests,json, queue, threading, time
import faster_than_requests as frequests
from hmac_headers import HMACHeaders
from login import AccessToken
import uuid, datetime, os, gzip, http.client

sms_send_url = "https://api.sms.to/v1/sms/send"

def log_message(log):
    print(log)


class Main(HMACHeaders, AccessToken):

    def __init__(self):
        HMACHeaders.__init__(self)
        AccessToken.__init__(self)

        self.response_status = []
        self.offerIds = set()
        self.auto_stop = False
        self.flex_instance_id = ''

    def is_next_day(self, timestamp):
        block_datetime = datetime.datetime.fromtimestamp(timestamp)
        if block_datetime.date() > datetime.datetime.today().date():
            return True
        return False

    def wait_for_minutes(self, sec):
        stime = time.time_ns()
        diff = 0
        nsec = sec * 1000000000;
        while diff < nsec:
            diff = ((time.time_ns() - stime))
            try:
                frequests.gets("https://postman-echo.com/get?foo1=bar1&foo2=bar2")
            except:
                print('')
            time.sleep(5)

    def check_blocks(self):
        min_amount = int(self.settings.get('MIN_AMOUNT'))
        next_day = self.settings.get('NEXT_DAY') == 'YES'
        running_start_time = time.time_ns()
        self.wait_for_minutes(0)
        now = datetime.datetime.now()
        is_proxy = False
        while 1:
            refresh_headers = {}
            hmac_header = self.new_post_getoffer(self.access_token)
            refresh_headers.update(hmac_header)
            refresh_headers.update(self.headers)
            refresh_headers.update(self.base_headers)
            service_area_id = self.settings.get('SERVICE_AREA_ID')
            request_data = {"apiVersion":"V2", "serviceAreaIds":[service_area_id]}

            tuple_header = list(refresh_headers.items())
            frequests.setHeaders(tuple_header)
            start_time = time.time_ns()

            if is_proxy:
                if (datetime.datetime.now() - now).total_seconds() > self.settings.get('MINS_P') * 60:
                    now = datetime.datetime.now()
                    is_proxy = False
            else:
                if (datetime.datetime.now() - now).total_seconds() > self.settings.get('MINS_R') * 60:
                    now = datetime.datetime.now()
                    is_proxy = True

            if not is_proxy:
                # check_block_resp = frequests.posts(self.refresh_post_block_url, json.dumps(request_data))
                check_block_resp = frequests.requests(self.refresh_post_block_url, body=json.dumps(request_data), http_method="post", http_headers=tuple_header)
            else:
                check_block_resp = frequests.requests2(self.refresh_post_block_url, body=json.dumps(request_data), proxyUrl="http://38.240.185.236:14888", proxyAuth="proxy202159:8WgDGspJ", userAgent="", timeout=9000, maxRedirects=9, http_method="post", http_headers=tuple_header)

            time_consumed = time.time_ns() - start_time

            response_code = int(check_block_resp["status"][0:3])

            if response_code == 200:
                self.response_status.append(True)

            else:
                log_message("%-20s : %d" % ("  Status code", response_code))
                if response_code == 400:
                    self.response_status.append(False)


            self.response_status = self.response_status[-self.settings.get('ERROR_QUANTITY'):]
            if len(self.response_status) == self.settings.get('ERROR_QUANTITY') and len([x for x in self.response_status if x]) == 0:
                log_message("%-20s" % "Too many 400s: sleeping for some minutes")
                self.response_status = []
                open("paused.txt", "w+")
                self.wait_for_minutes(self.settings.get('REQ_SLEEP_TIME'))
                os.remove('paused.txt')
                running_start_time = time.time_ns()
                continue

            if response_code == 401:
                print("script is terminated")
                quit()

            if response_code == 403:
                EMAIL = self.settings.get('EMAIL')
                PASSWORD = self.settings.get('PASSWORD')
                SOURCE_TOKEN = self.settings.get('SOURCE_TOKEN')
                access_token = self.update_token(EMAIL, PASSWORD, SOURCE_TOKEN)
                log_message(access_token)
                if access_token:
                    self.access_token = access_token
                self.response_status = []
                continue

            if response_code == 400:
                time.sleep(1)
                continue

            raw_body = check_block_resp["body"]
            resp_body = {}

            try:
                resp_body = json.loads(gzip.decompress(raw_body))
            except:
                resp_body = json.loads(raw_body)

            offers_list = resp_body["offerList"] #check_block_resp.json()['offerList']
            final_offers = []

            final_offers = [each_offer for each_offer in offers_list \
                if each_offer['serviceAreaId'] in self.settings['FILTER_SIDS'] and \
                each_offer['rateInfo']['priceAmount'] >= min_amount and \
                (not next_day or self.is_next_day(each_offer['startTime']))]

            if (time.time_ns() - running_start_time)/1000000000 > self.settings.get('RUNNING_TIME'):
                log_message("%-20s" % "Too long running: sleeping for some minutes")
                self.response_status = []
                open("paused.txt", "w+")
                self.wait_for_minutes(self.settings.get('RUNNING_TIME'))
                os.remove('paused.txt')
                running_start_time = time.time_ns()
                continue

            offers_list = [offer for offer in offers_list if offer['offerId'] not in self.offerIds]

            if self.auto_stop:
                break

            elif self.settings.get('SLEEP_TIME') > time_consumed/1000000000:
               time.sleep(self.settings.get('SLEEP_TIME')-time_consumed/1000000000)

            log_message("%-20s : %fs" % ("  Time consumed", time_consumed/1000000000))

            if len(offers_list) > 0:
                with open('offers.txt', 'a+', encoding='utf-8') as (save_offers):
                    for offer in offers_list:
                        save_offers.write(str(offer))
                        save_offers.write("\n")
                        self.offerIds.add(offer['offerId'])


    def cb_wrapper(self):
        while True:
            if self.auto_stop:
                break
            try:
                self.check_blocks()
            except Exception as e:
                print(e)

                with open('errors.txt', 'a+') as (t):
                    t.write(str(e))
                    t.write("\n")


    def start_bot(self):

        EMAIL = self.settings.get('EMAIL')
        PASSWORD = self.settings.get('PASSWORD')
        SOURCE_TOKEN = self.settings.get('SOURCE_TOKEN')
        access_token = self.update_token(EMAIL, PASSWORD, SOURCE_TOKEN)

        self.flex_instance_id = self.settings.get('FLEX_INSTANCE_ID')
        if access_token:
            self.access_token = access_token
            log_message("%-20s : %s" % ("AccessToken", access_token))
            for i in range(int(self.settings.get('THREADS'))):
                th = threading.Thread(target=self.cb_wrapper)
                th.start()

    def send_email(self, offer_resp):
        send_to_emails = self.settings.get('NOTIFICATION_EMAILS')
        ct = datetime.datetime.now()

        ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[((n//10)%10!=1)*(n%10<4)*n%10::4])
        day = int(datetime.datetime.strftime(ct, '%d'))

        message = ('Offer Caught on {0}{1}{2}').format(datetime.datetime.strftime(ct, '%A, %B '),
                ordinal(day),
                datetime.datetime.strftime(ct, ' at %I:%M %p'))

        conn = http.client.HTTPSConnection("api.sms.to")
        payload = "{\"body\": \"" + message + "\",\"to\": \"" + send_to_emails + "\",\"sender_id\": \"17607607932\",\"callback_url\": \"http://turbotflex.com\"}"
        print(payload)

        headers = {
            'Authorization': "Bearer OwQuiPLDlJxmrGUPhWrATWRhN425o3yK",
            'Content-Type': "application/json",
            'Accept': "application/json"
        }
        print("SMS Sending " + sms_send_url)
        conn.request("POST", "/v1/sms/send", payload, headers)

        response = conn.getresponse()
        if response.status == 200:
            print('Sent')
            data = response.read()
            print(data.decode("utf-8"))
        else:
            print("%d: %s" % (response.status, response.reason))


alpha = Main()
alpha.start_bot()
